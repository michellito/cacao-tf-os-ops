variable "username" {
  type        = string
  description = "name of user"
}

variable "region" {
  type = string
  description = "string, openstack region name; default = IU"
  default = "IU"
}

variable "external_network_uuid" {
  type        = string
  description = "uuid of the external network"
}

variable "tenant_cidr" {
  type        = string
  description = "private cidr to use for tenant network"
  default     = "10.0.0.0/24"
}

variable "vnc_ip_ranges" {
  type        = any
  description = "array of ranges of ips, either as a comma-separated string or a list(string), in ingress that can attempt to connect via vnc"
  default     = ["0.0.0.0/0"]
}

variable "keypair_name" {
  type        = string
  description = "name of the keypair"
  default     = "cacao-ssh-key"
}

variable "public_ssh_key" {
  type        = string
  description = "public ssh key to use to create keypair"
}
