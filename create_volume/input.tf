variable "region" {
  type = string
  description = "string, openstack region name; default = IU"
  default = "IU"
}

variable "volume_name" {
  type = string
  description = "name of volume as seen in openstack cli"
}

variable "volume_size" {
  type = string
  description = "size of volume in gb"
}
